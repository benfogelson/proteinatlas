import os
import functools

import tensorflow as tf
import numpy as np
from PIL import Image
import tables
import dask

from tensorflow.python.keras import backend as K

from utils import Params

CHANNELS = ['red', 'green', 'blue', 'yellow']


def get_dataset_png(df, image_dir, is_training, params):
    image_dir = tf.constant(image_dir)

    png_parse_fn = lambda name, label: png_parse_function(name, label, image_dir, is_training, params)

    names = df.Id.values[:, np.newaxis]
    labels = df.drop(columns=['Id', 'splitting'])

    n_samples = labels.shape[0]

    names = tf.constant(names, name='names')
    labels = tf.constant(labels, name='labels')

    labels = tf.cast(labels, K.floatx())

    ds = tf.data.Dataset.from_tensor_slices((names, labels))
    if is_training:
        ds = ds.shuffle(n_samples)
    ds = ds.map(png_parse_fn, num_parallel_calls=params.num_parallel_calls)
    ds = ds.batch(params.batch_size)
    ds = ds.prefetch(1)

    return ds


def get_dataset_h5(group, is_training, params, predict=False):
    labels = group.labels   # type: tables.CArray

    n_samples = len(labels)

    indices = np.array(range(n_samples), dtype=np.int32)
    indices = tf.constant(indices)

    h5_parse_fn_py = lambda idx: h5_parse_function_py(idx, group)
    h5_parse_fn = lambda idx: tf.py_func(h5_parse_fn_py, [idx], (tf.uint8, tf.uint8))

    train_parse_fn = lambda X, y: train_parse_function(X, y, params)
    eval_parse_fn = lambda X, y: eval_parse_function(X, y, params)

    ds = tf.data.Dataset.from_tensor_slices(indices)
    if is_training:
        ds = ds.shuffle(n_samples)
    ds = ds.map(h5_parse_fn)
    if is_training:
        ds = ds.map(train_parse_fn, num_parallel_calls=params.num_parallel_calls)
    else:
        ds = ds.map(eval_parse_fn, num_parallel_calls=params.num_parallel_calls)
    ds = ds.batch(params.batch_size)
    ds = ds.prefetch(1)
    if not predict:
        ds = ds.repeat()

    return ds


def get_dataset_synthetic(params):
    n_labels = 28
    batch_size = params.batch_size
    image_size = params.image_size
    n_channels = 4

    dtype = getattr(tf, params.dtype)

    def parse_fn(idx):
        image = tf.ones((n_channels, image_size, image_size), dtype=dtype, name='image')
        labels = tf.ones((n_labels,), dtype=dtype, name='labels')

        return image, labels

    indices = np.arange(0, 10*batch_size)
    indices = tf.constant(indices)

    ds = tf.data.Dataset.from_tensor_slices(indices)
    ds = ds.map(parse_fn, num_parallel_calls=params.num_parallel_calls)
    ds = ds.batch(batch_size)
    ds = ds.repeat()
    ds = ds.prefetch(1)

    return ds


def png_parse_function(name, label, image_dir, is_training, params):
    image_stack = []
    for channel in CHANNELS:
        file_name = image_dir + '/' + tf.squeeze(name) + '_' + channel + '.png'
        image = tf.read_file(file_name)
        image = tf.image.decode_png(image, channels=1)
        image = tf.cast(image, K.floatx())/255
        image_stack.append(image)
    image_stack = tf.concat(image_stack, -1)

    image_stack = tf.image.resize_images(image_stack, (params.image_size, params.image_size),
                                         method=tf.image.ResizeMethod.BICUBIC)

    if is_training:
        image_stack = tf.image.random_flip_up_down(image_stack)
        image_stack = tf.image.random_flip_left_right(image_stack)

    if K.image_data_format() == 'channels_first':
        image_stack = tf.transpose(image_stack)

    return image_stack, label


def h5_parse_function_py(indices, group):
    X = group.images[indices, :, :, :]
    y = group.labels[indices, :]

    return X, y


def train_parse_function(X, y, params):
    X, y = reshape_fn(X, y)

    X = tf.cast(X, params.dtype)/255.
    y = tf.cast(y, params.dtype)

    X = tf.transpose(X)
    X = tf.image.resize_images(X, (params.image_size, params.image_size))
    X = tf.image.random_flip_up_down(X)
    X = tf.image.random_flip_left_right(X)
    X = tf.transpose(X)

    return X, y


def eval_parse_function(X, y, params):
    X, y = reshape_fn(X, y)

    X = tf.cast(X, tf.float16)/255.
    y = tf.cast(y, tf.int16)

    X = tf.transpose(X)
    X = tf.image.resize_images(X, (params.image_size, params.image_size))
    X = tf.transpose(X)

    return X, y


def reshape_fn(X, y):
    X = tf.reshape(X, (4, 512, 512))
    y = tf.reshape(y, (28,))
    return X, y

