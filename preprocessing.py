import collections
import logging
import os

import click
import dask
import tables
import numpy as np
import pandas as pd
import tqdm
from PIL import Image
from skmultilearn.model_selection import iterative_train_test_split


@click.group(chain=True)
@click.pass_context
@click.option('--seed', default=32087)
@click.option('--debug/--no-debug', default=False)
def cli(ctx, seed, debug):
    if debug:
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
    ctx.obj = {}
    np.random.seed(seed)


@cli.command()
@click.pass_context
@click.argument('csv', type=click.File())
@click.argument('eval_frac', type=click.FLOAT)
@click.argument('test_frac', type=click.FLOAT)
def make_split(ctx, csv: click.File, eval_frac, test_frac):
    assert eval_frac + test_frac < 1
    df = pd.read_csv(csv)

    targets = df.Target.apply(lambda s: [int(t) for t in s.split()]).apply(collections.Counter)
    targets = pd.DataFrame.from_records(targets).fillna(0)

    df = df.drop(columns=['Target'])
    df = df.join(targets)

    X = df.index.values[:, np.newaxis]
    y = df.drop(columns=['Id']).values

    X_train, y_train, X_eval_test, y_eval_test = iterative_train_test_split(
        X, y, eval_frac + test_frac
    )

    X_eval, y_eval, X_test, y_test = iterative_train_test_split(
        X_eval_test, y_eval_test, test_frac / (eval_frac + test_frac)
    )

    ctx.obj['X_train'] = X_train
    ctx.obj['y_train'] = y_train
    ctx.obj['names_train'] = df.loc[X_train.flatten(), 'Id'].values
    ctx.obj['X_eval'] = X_eval
    ctx.obj['y_eval'] = y_eval
    ctx.obj['names_eval'] = df.loc[X_eval.flatten(), 'Id'].values
    ctx.obj['X_test'] = X_test
    ctx.obj['y_test'] = y_test
    ctx.obj['names_test'] = df.loc[X_test.flatten(), 'Id'].values

    csv_out = csv.name[:-4] + '_split.csv'

    df.loc[:, 'splitting'] = ''
    df.loc[X_train.flatten(), 'splitting'] = 'train'
    df.loc[X_eval.flatten(), 'splitting'] = 'eval'
    df.loc[X_test.flatten(), 'splitting'] = 'test'

    df = df.sort_values('splitting', ascending=False)

    df.to_csv(csv_out)


@cli.command()
@click.pass_context
@click.argument('image_dir', type=click.Path())
@click.option('--output', '-o', default='./split_training_data.h5')
@click.option('--complib', default='zlib')
@click.option('--complevel', default=5)
@click.option('--imageformat', default='channels_first')
def build_split_h5(ctx, image_dir, output, complib, complevel, imageformat):
    h5_file = tables.open_file(output, mode='w')

    for group_name in ['test', 'eval', 'train']:
        group = h5_file.create_group('/', group_name)
        X = ctx.obj['X_' + group_name]
        y = ctx.obj['y_' + group_name]
        sample_names = ctx.obj['names_' + group_name]

        _build_h5(X, y, sample_names, image_dir, h5_file, group, complib, complevel, imageformat)


@cli.command()
@click.pass_context
@click.argument('submission_template')
@click.argument('image_dir', type=click.Path())
@click.option('--output', '-o', default='./test_images.h5')
@click.option('--complib', default='zlib')
@click.option('--complevel', default=5)
@click.option('--imageformat', default='channels_first')
def build_test_h5(ctx, submission_template, image_dir, output, complib, complevel, imageformat):
    h5_file = tables.open_file(output, mode='w')

    df = pd.read_csv(submission_template)
    group = h5_file.root

    n_classes = 28
    n_samples = len(df)

    y = np.zeros((n_samples, n_classes))

    _build_h5(None, y, df.Id.values, image_dir, h5_file, group, complib, complevel, imageformat)


def _build_h5(X, y, sample_names, image_dir, h5_file, group, complib, complevel, imageformat):
    assert imageformat == 'channels_first' or imageformat == 'channels_last'

    n_classes = y.shape[1]
    n_samples = y.shape[0]

    image_size = 512
    n_channels = 4

    if imageformat == 'channels_first':
        channels_first = True
        image_shape = (n_channels, image_size, image_size)
    else:
        channels_first = False
        image_shape = (image_size, image_size, n_channels)

    filters = tables.Filters(complib=complib, complevel=complevel)

    images = h5_file.create_carray(group, 'images',
                                   tables.UInt8Atom(),
                                   shape=(n_samples, *image_shape),
                                   filters=filters,
                                   chunkshape=(1,*image_shape))

    labels = h5_file.create_carray(group, 'labels',
                                   tables.UInt8Atom(),
                                   shape=(n_samples, n_classes),
                                   filters=filters)

    names = h5_file.create_carray(group, 'names',
                                  tables.StringAtom(itemsize=36),
                                  shape=(n_samples,),
                                  filters=filters)

    dask_batch_size = 32
    n_dask_batches = int(np.floor(n_samples / dask_batch_size))

    remainder = n_samples - n_dask_batches*dask_batch_size
    if remainder > 0:
        n_dask_batches += 1

    for batch in tqdm.tqdm(range(n_dask_batches)):
        i0 = dask_batch_size * batch
        i1 = min(dask_batch_size * (batch + 1), n_samples)

        image_array = np.empty((i1 - i0, *image_shape), dtype=np.uint8)

        def extract_image(i_loc, channel, file_name):
            im = Image.open(file_name)
            im = np.array(im)
            if channels_first:
                image_array[i_loc, channel, :, :] = im
            else:
                image_array[i_loc, :, :, channel] = im

        dask_computations = []
        for i in range(i0, i1):
            i_loc = i - i0
            sample_name = sample_names[i]
            for channel, color in enumerate(['red', 'blue', 'yellow', 'green']):
                file_name = '{}_{}.png'.format(sample_name, color)
                file_name = os.path.join(image_dir, file_name)

                dask_computations.append(
                    dask.delayed(extract_image)(i_loc, channel, file_name)
                )
        dask.compute(dask_computations, num_workers=4)
        images[i0:i1, :, :, :] = image_array
        labels[i0: i1, :] = y[i0: i1, :]
        names[i0: i1] = sample_names[i0: i1]


if __name__ == '__main__':
    cli()
