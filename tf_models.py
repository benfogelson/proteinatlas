import functools

import tensorflow as tf
from tensorflow.contrib.layers import batch_norm, l2_regularizer
from tensorflow.contrib.mixed_precision.python.loss_scale_optimizer import LossScaleOptimizer
from tensorflow.contrib.mixed_precision.python.loss_scale_manager import FixedLossScaleManager
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import array_ops

import utils
import tf_metrics
import tf_resnet

from tensorflow.python.keras.applications import ResNet50


def float32_variable_storage_getter(getter, name, shape=None, dtype=None,
                                    initializer=None, regularizer=None,
                                    trainable=True,
                                    *args, **kwargs):
    storage_dtype = tf.float32 if trainable else dtype
    variable = getter(name, shape, dtype=storage_dtype,
                      initializer=initializer, regularizer=regularizer,
                      trainable=trainable,
                      *args, **kwargs)
    if trainable and dtype != tf.float32:
        variable = tf.cast(variable, dtype)
    return variable


def conv2d_layer(filters, x, scope_name, regularizer=None):
    with tf.variable_scope(scope_name) as sc:
        x = tf.layers.conv2d(x, filters, (3, 3), padding='valid', data_format='channels_first',
                             kernel_regularizer=regularizer, bias_regularizer=regularizer,
                             reuse=tf.AUTO_REUSE)
        x = batch_norm(x, data_format='NCHW', reuse=tf.AUTO_REUSE, scope=sc)
        x = tf.nn.relu(x)
    return x


def dense_layer(nodes, x, scope_name, regularizer=None):
    with tf.variable_scope(scope_name) as sc:
        x = tf.layers.dense(x, nodes, kernel_regularizer=regularizer, bias_regularizer=regularizer, reuse=tf.AUTO_REUSE)
        x = batch_norm(x, data_format='NCHW', reuse=tf.AUTO_REUSE, scope=sc)
        x = tf.nn.relu(x)
    return x


def shallow_model_fn(features, labels, mode, params):
    sc = tf.get_variable_scope()
    sc.set_custom_getter(float32_variable_storage_getter)

    x = tf.cast(features, params.dtype)
    x = tf.layers.flatten(x)
    x = tf.layers.dense(x, 128, activation='relu')
    x = batch_norm(x)
    logits = tf.layers.dense(x, 28)

    labels = tf.cast(labels, params.dtype)

    if mode == tf.estimator.ModeKeys.PREDICT:
        raise NotImplementedError

    loss = tf.losses.sigmoid_cross_entropy(labels, logits)

    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer()
        manager = FixedLossScaleManager(1e3)
        optimizer = LossScaleOptimizer(optimizer, manager)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step()
        )
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    if mode == tf.estimator.ModeKeys.EVAL:
        raise NotImplementedError


def _predict_with_tta(logits_fn, x, labels, mode, params):
    # Transpose to channels_last
    x = tf.transpose(x, [0, 2, 3, 1])
    inputs = [
        x,
        tf.image.flip_left_right(x),
        tf.image.flip_up_down(x),
        tf.image.flip_up_down(tf.image.flip_left_right(x))
    ]

    # Tranpose back to channels_first
    inputs = [tf.transpose(i, [0, 3, 1, 2]) for i in inputs]

    outputs = [_predict(logits_fn, i, labels, mode, params) for i in inputs]
    _, predictions_list = zip(*outputs)

    probabilities = tf.add_n([p['probabilities'] for p in predictions_list])/len(inputs)

    threshold = tf.constant(0.5, dtype=probabilities.dtype)
    threshold_condition = math_ops.greater(probabilities, threshold)

    max_probability = tf.reshape(tf.reduce_max(probabilities, reduction_indices=[1]), (-1, 1))
    max_probability_condition = math_ops.equal(probabilities, max_probability)

    condition = math_ops.logical_or(threshold_condition, max_probability_condition)
    zero = array_ops.zeros_like(probabilities)
    one = array_ops.ones_like(probabilities)
    labels = array_ops.where(condition, one, zero)

    predictions = dict(
        probabilities=probabilities,
        labels=labels
    )

    logits = math_ops.log(probabilities/(1-probabilities))

    return logits, predictions


def _predict(logits_fn, x, labels, mode, params):
    logits = logits_fn(x, labels, mode, params)

    probabilities = tf.nn.sigmoid(logits)

    threshold = tf.constant(0.5, dtype=probabilities.dtype)
    threshold_condition = math_ops.greater(probabilities, threshold)

    max_probability = tf.reshape(tf.reduce_max(probabilities, reduction_indices=[1]), (-1, 1))
    max_probability_condition = math_ops.equal(probabilities, max_probability)

    condition = math_ops.logical_or(threshold_condition, max_probability_condition)
    zero = array_ops.zeros_like(probabilities)
    one = array_ops.ones_like(probabilities)
    labels = array_ops.where(condition, one, zero)

    predictions = dict(
        probabilities=probabilities,
        labels=labels
    )

    return logits, predictions


def mixed_precision_model_fn(logits_fn):
    @functools.wraps(logits_fn)
    def model_fn(features, labels, mode, params, config):
        sc = tf.get_variable_scope()
        sc.set_custom_getter(float32_variable_storage_getter)

        x = tf.cast(features, params.dtype)

        if mode == tf.estimator.ModeKeys.TRAIN or not params.tta:
            logits, predictions = _predict(logits_fn, x, labels, mode, params)
        else:
            logits, predictions = _predict_with_tta(logits_fn, x, labels, mode, params)

        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

        labels = tf.cast(labels, params.dtype)

        loss = tf.losses.sigmoid_cross_entropy(labels, logits)
        loss += tf.cast(tf.losses.get_regularization_loss(), loss.dtype)

        if mode == tf.estimator.ModeKeys.TRAIN:
            optimizer = tf.train.AdamOptimizer()
            manager = FixedLossScaleManager(params.loss_scale)
            optimizer = LossScaleOptimizer(optimizer, manager)
            train_op = optimizer.minimize(
                loss=loss,
                global_step=tf.train.get_global_step()
            )
            return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

        if mode == tf.estimator.ModeKeys.EVAL:

            tf.estimator.BestExporter()

            counts, up_counts = tf_metrics.streaming_counts(labels, predictions['labels'], 28)
            f1, up_f1 = tf_metrics.streaming_f1(counts, up_counts)

            eval_metric_ops = {
                'true_positives': tf.metrics.true_positives(labels, predictions['labels']),
                'false_positives': tf.metrics.false_positives(labels, predictions['labels']),
                'true_negatives': tf.metrics.true_negatives(labels, predictions['labels']),
                'false_negatives': tf.metrics.false_negatives(labels, predictions['labels']),
                'f1': (f1, up_f1)
            }

            return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

    return model_fn


@mixed_precision_model_fn
def resnet50_model_fn(features, labels, mode, params):
    with tf.variable_scope('resnet_50', reuse=tf.AUTO_REUSE):
        logits = tf_resnet.resnet50_model_fn(features)
        return logits


@mixed_precision_model_fn
def deep_yeast_gap_model_fn(features, labels, mode, params):
    with tf.variable_scope('deep_yeast_gap'):
        x = _deep_yeast_conv_layers(features)

        with tf.variable_scope('global_average_pooling'):
            x = math_ops.reduce_mean(x, axis=[2, 3], keepdims=False)

        # x = tf.layers.flatten(x)
        logits = tf.layers.dense(x, 28, reuse=tf.AUTO_REUSE)

        return logits


def _deep_yeast_conv_layers(x):
    with tf.variable_scope('conv_group_0'):
        x = conv2d_layer(64, x, "conv0")
        x = conv2d_layer(64, x, "conv1")
        x = tf.layers.max_pooling2d(x, strides=(2, 2), pool_size=(2, 2), padding='valid', data_format='channels_first')
    with tf.variable_scope('conv_group_1'):
        x = conv2d_layer(128, x, "conv0")
        x = conv2d_layer(128, x, "conv1")
        x = tf.layers.max_pooling2d(x, strides=(2, 2), pool_size=(2, 2), padding='valid', data_format='channels_first')
    with tf.variable_scope('conv_group_3'):
        x = conv2d_layer(256, x, "conv0")
        x = conv2d_layer(256, x, "conv1")
        x = conv2d_layer(256, x, "conv2")
        x = conv2d_layer(256, x, "conv3")
        x = tf.layers.max_pooling2d(x, strides=(2, 2), pool_size=(2, 2), padding='valid', data_format='channels_first')
    return x


@mixed_precision_model_fn
def deep_yeast_model_fn(features, labels, mode, params):
    with tf.name_scope('deep_yeast'):
        x = _deep_yeast_conv_layers(features)

        with tf.name_scope('fc'):
            x = tf.layers.flatten(x)
            x = dense_layer(512, x)
            x = tf.layers.dropout(x, training=(mode == tf.estimator.ModeKeys.TRAIN))
            x = dense_layer(512, x)
            x = tf.layers.dropout(x, training=(mode == tf.estimator.ModeKeys.TRAIN))
            x = dense_layer(512, x)
        logits = tf.layers.dense(x, 28)

        return logits


