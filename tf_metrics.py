"""
From https://vict0rs.ch/2018/06/06/tensorflow-streaming-multilabel-f1/
"""

import tensorflow as tf

from tensorflow.python.ops.metrics import metric_variable


def streaming_counts(y_true, y_pred, n_classes):
    # Counts for the macro f1 score
    tp = metric_variable(
        shape=[n_classes], dtype=tf.int64, validate_shape=False, name="tp"
    )
    fp = metric_variable(
        shape=[n_classes], dtype=tf.int64, validate_shape=False, name="fp"
    )
    fn = metric_variable(
        shape=[n_classes], dtype=tf.int64, validate_shape=False, name="fn"
    )

    # Update ops, as in the previous section:
    #   - Update ops for the macro f1 score
    up_tp = tf.assign_add(tp, tf.count_nonzero(y_pred * y_true, axis=0))
    up_fp = tf.assign_add(fp, tf.count_nonzero(y_pred * (y_true - 1), axis=0))
    up_fn = tf.assign_add(fn, tf.count_nonzero((y_pred - 1) * y_true, axis=0))

    # Grouping values
    counts = (tp, fp, fn)
    updates = tf.group(up_tp, up_fp, up_fn)

    return counts, updates


def streaming_f1(counts, up_counts):
    # unpacking values
    tp, fp, fn = counts

    f1 = metric_variable(
        shape=(), dtype=tf.float64, validate_shape=False, name="f1"
    )

    with tf.control_dependencies([up_counts]):
        tp = tf.cast(tp, tf.float64)
        fp = tf.cast(fp, tf.float64)
        fn = tf.cast(fn, tf.float64)
        prec = tf.div_no_nan(tp, (tp + fp))
        rec = tf.div_no_nan(tp, (tp + fn))

        up_f1 = tf.assign(f1, tf.reduce_mean(2 * tf.div_no_nan(prec * rec, (prec + rec))))

    return f1, up_f1
