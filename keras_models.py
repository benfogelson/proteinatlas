import itertools

import tensorflow as tf
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Conv2D, Input, Lambda, MaxPooling2D, BatchNormalization, Activation, Dense,\
    Flatten, Dropout

from tf_models import float32_variable_storage_getter

from tensorflow.python.eager import context
from tensorflow.python.keras import backend as K
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.utils import tf_utils
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn


class MixedPrecisionBatchNormalization(BatchNormalization):
    def call(self, inputs, training=None):

        # Raise inputs to higher precision
        input_dtype = inputs.dtype
        if input_dtype == tf.float16 or input_dtype == tf.bfloat16:
            inputs = tf.cast(inputs, tf.float32)

        original_training_value = training
        if training is None:
            training = K.learning_phase()

        in_eager_mode = context.executing_eagerly()
        if self.virtual_batch_size is not None:
            # Virtual batches (aka ghost batches) can be simulated by reshaping the
            # Tensor and reusing the existing batch norm implementation
            original_shape = [-1] + inputs.shape.as_list()[1:]
            expanded_shape = [self.virtual_batch_size, -1] + original_shape[1:]

            # Will cause errors if virtual_batch_size does not divide the batch size
            inputs = array_ops.reshape(inputs, expanded_shape)

            def undo_virtual_batching(outputs):
                outputs = array_ops.reshape(outputs, original_shape)
                return outputs

        if self.fused:
            outputs = self._fused_batch_norm(inputs, training=training)
            if self.virtual_batch_size is not None:
                # Currently never reaches here since fused_batch_norm does not support
                # virtual batching
                outputs = undo_virtual_batching(outputs)
            if not context.executing_eagerly() and original_training_value is None:
                outputs._uses_learning_phase = True  # pylint: disable=protected-access
            outputs = tf.cast(outputs, input_dtype)
            return outputs

        # Compute the axes along which to reduce the mean / variance
        input_shape = inputs.get_shape()
        ndims = len(input_shape)
        reduction_axes = [i for i in range(ndims) if i not in self.axis]
        if self.virtual_batch_size is not None:
            del reduction_axes[1]  # Do not reduce along virtual batch dim

        # Broadcasting only necessary for single-axis batch norm where the axis is
        # not the last dimension
        broadcast_shape = [1] * ndims
        broadcast_shape[self.axis[0]] = input_shape[self.axis[0]].value

        def _broadcast(v):
            if (v is not None and
                    len(v.get_shape()) != ndims and
                    reduction_axes != list(range(ndims - 1))):
                return array_ops.reshape(v, broadcast_shape)
            return v

        scale, offset = _broadcast(self.gamma), _broadcast(self.beta)

        def _compose_transforms(scale, offset, then_scale, then_offset):
            if then_scale is not None:
                scale *= then_scale
                offset *= then_scale
            if then_offset is not None:
                offset += then_offset
            return (scale, offset)

        # Determine a boolean value for `training`: could be True, False, or None.
        training_value = tf_utils.constant_value(training)
        if training_value is not False:
            if self.adjustment:
                adj_scale, adj_bias = self.adjustment(array_ops.shape(inputs))
                # Adjust only during training.
                adj_scale = tf_utils.smart_cond(training,
                                                lambda: adj_scale,
                                                lambda: array_ops.ones_like(adj_scale))
                adj_bias = tf_utils.smart_cond(training,
                                               lambda: adj_bias,
                                               lambda: array_ops.zeros_like(adj_bias))
                scale, offset = _compose_transforms(adj_scale, adj_bias, scale, offset)

            # Some of the computations here are not necessary when training==False
            # but not a constant. However, this makes the code simpler.
            keep_dims = self.virtual_batch_size is not None or len(self.axis) > 1
            mean, variance = nn.moments(inputs, reduction_axes, keep_dims=keep_dims)

            moving_mean = self.moving_mean
            moving_variance = self.moving_variance

            mean = tf_utils.smart_cond(training,
                                       lambda: mean,
                                       lambda: moving_mean)
            variance = tf_utils.smart_cond(training,
                                           lambda: variance,
                                           lambda: moving_variance)

            if self.virtual_batch_size is not None:
                # This isn't strictly correct since in ghost batch norm, you are
                # supposed to sequentially update the moving_mean and moving_variance
                # with each sub-batch. However, since the moving statistics are only
                # used during evaluation, it is more efficient to just update in one
                # step and should not make a significant difference in the result.
                new_mean = math_ops.reduce_mean(mean, axis=1, keepdims=True)
                new_variance = math_ops.reduce_mean(variance, axis=1, keepdims=True)
            else:
                new_mean, new_variance = mean, variance

            if self.renorm:
                r, d, new_mean, new_variance = self._renorm_correction_and_moments(
                    new_mean, new_variance, training)
                # When training, the normalized values (say, x) will be transformed as
                # x * gamma + beta without renorm, and (x * r + d) * gamma + beta
                # = x * (r * gamma) + (d * gamma + beta) with renorm.
                r = _broadcast(array_ops.stop_gradient(r, name='renorm_r'))
                d = _broadcast(array_ops.stop_gradient(d, name='renorm_d'))
                scale, offset = _compose_transforms(r, d, scale, offset)

            def _do_update(var, value):
                if in_eager_mode and not self.trainable:
                    return

                return self._assign_moving_average(var, value, self.momentum)

            mean_update = tf_utils.smart_cond(
                training,
                lambda: _do_update(self.moving_mean, new_mean),
                lambda: self.moving_mean)
            variance_update = tf_utils.smart_cond(
                training,
                lambda: _do_update(self.moving_variance, new_variance),
                lambda: self.moving_variance)
            if not context.executing_eagerly():
                self.add_update(mean_update, inputs=True)
                self.add_update(variance_update, inputs=True)

        else:
            mean, variance = self.moving_mean, self.moving_variance

        mean = math_ops.cast(mean, inputs.dtype)
        variance = math_ops.cast(variance, inputs.dtype)
        if scale is not None:
            scale = math_ops.cast(offset, inputs.dtype)
        if offset is not None:
            offset = math_ops.cast(offset, inputs.dtype)
        outputs = nn.batch_normalization(inputs,
                                         _broadcast(mean),
                                         _broadcast(variance),
                                         offset,
                                         scale,
                                         self.epsilon)

        outputs = tf.cast(outputs, input_dtype)

        # If some components of the shape got lost due to adjustments, fix that.
        outputs.set_shape(input_shape)

        if self.virtual_batch_size is not None:
            outputs = undo_virtual_batching(outputs)
        if not context.executing_eagerly() and original_training_value is None:
            outputs._uses_learning_phase = True  # pylint: disable=protected-access
        return outputs


def conv2d_layer(filters, x):
    x = Conv2D(filters, (3, 3), padding='same',
               kernel_regularizer=regularizers.l2(0.01),
               activity_regularizer=regularizers.l1(0.01))(x)
    x = BatchNormalization(1)(x)
    x = Activation('relu')(x)
    return x


def dense_layer(nodes, x):
    x = Dense(nodes,
              kernel_regularizer=regularizers.l2(0.01),
              activity_regularizer=regularizers.l1(0.01))(x)
    x = BatchNormalization(1)(x)
    x = Activation('relu')(x)
    return x


def deep_yeast_model(params):
    scope = tf.get_variable_scope()
    scope.set_custom_getter(float32_variable_storage_getter)
    x_input = Input((4, params.image_size, params.image_size))
    x = conv2d_layer(64, x_input)
    x = conv2d_layer(64, x)
    x = MaxPooling2D(strides=(2, 2))(x)
    x = conv2d_layer(128, x)
    x = conv2d_layer(128, x)
    x = MaxPooling2D(strides=(2, 2))(x)
    x = conv2d_layer(256, x)
    x = conv2d_layer(256, x)
    x = conv2d_layer(256, x)
    x = conv2d_layer(256, x)
    x = MaxPooling2D(strides=(2, 2))(x)
    x = Flatten()(x)
    x = dense_layer(512, x)
    x = Dropout(0.5)(x)
    x = dense_layer(512, x)
    x = Dropout(0.5)(x)
    x = dense_layer(512, x)
    x = Dense(28)(x)
    x = Activation('sigmoid')(x)
    model = Model(inputs=x_input, outputs=x)
    return model


def stacked_models(base_model: Model, params):
    n_channels = 4
    X = Input((4, params.image_size, params.image_size))

    channels = [0, 1, 2, 3]
    inputs = []
    for permutation in itertools.permutations(channels, 3):
        inputs.append(
            Lambda(
                lambda x:
                    tf.stack([x[:, i, :, :] for i in permutation],axis=1)
            )(X))

    return inputs



# input_shape = (4, params.image_size, params.image_size)
#
# base_model = ResNet50(weights=None, input_shape=input_shape, include_top=False)
# x = base_model.output
# x = Flatten()(x)
# # x = GlobalAveragePooling2D()(x)
# x = Dense(1024, activation='relu')(x)
# x = Dense(n_classes, activation='sigmoid')(x)
#
# model = Model(inputs=base_model.input, outputs=x)
# for layer in base_model.layers:
#     layer.trainable = True
#
# ds_train = get_dataset(train, True, params)
# ds_eval = get_dataset(eval, False, params)
#
# model.compile(optimizer='adam',
#               loss='binary_crossentropy',
#               metrics=[MacroF1Score(n_classes, 0.5)]
#               )
# model.fit(ds_train,
#           steps_per_epoch=n_train // params.batch_size,
#           epochs=params.epochs)