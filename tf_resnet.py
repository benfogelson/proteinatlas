"""
Resnet implementation copied/adapted from keras_applications/resnet50, intended for mixed-precision use
"""
import tensorflow as tf
from tensorflow.contrib.layers import batch_norm


def identity_block(input_tensor, kernel_size, filters, stage, block):
    """The identity block is the block that has no conv layer at shortcut.

    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of
            middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names

    # Returns
        Output tensor for the block.
    """
    filters1, filters2, filters3 = filters

    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = tf.layers.conv2d(input_tensor,
                         filters1, (1, 1),
                         name=conv_name_base + '2a',
                         data_format='channels_first')
    with tf.variable_scope(bn_name_base + '2a'):
        x = batch_norm(x, data_format='NCHW')
    x = tf.nn.relu(x)

    x = tf.layers.conv2d(x,
                         filters2, kernel_size,
                         padding='same',
                         name=conv_name_base + '2b',
                         data_format='channels_first')
    with tf.variable_scope(bn_name_base + '2b'):
        x = batch_norm(x, data_format='NCHW')
    x = tf.nn.relu(x)

    x = tf.layers.conv2d(x,
                         filters3, (1, 1),
                         name=conv_name_base + '2c',
                         data_format='channels_first')
    with tf.variable_scope(bn_name_base + '2c'):
        x = batch_norm(x, data_format='NCHW')

    x = tf.math.add(x, input_tensor)
    x = tf.nn.relu(x)

    return x


def conv_block(input_tensor,
               kernel_size,
               filters,
               stage,
               block,
               strides=(2, 2)):
    """A block that has a conv layer at shortcut.

    # Arguments
        input_tensor: input tensor
        kernel_size: default 3, the kernel size of
            middle conv layer at main path
        filters: list of integers, the filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
        strides: Strides for the first conv layer in the block.

    # Returns
        Output tensor for the block.

    Note that from stage 3,
    the first conv layer at main path is with strides=(2, 2)
    And the shortcut should have strides=(2, 2) as well
    """
    filters1, filters2, filters3 = filters

    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = tf.layers.conv2d(input_tensor,
                         filters1, (1, 1), strides=strides,
                         name=conv_name_base + '2a',
                         data_format='channels_first')
    with tf.variable_scope(bn_name_base + '2a'):
        x = batch_norm(x, data_format='NCHW')
    x = tf.nn.relu(x)

    x = tf.layers.conv2d(x,
                         filters2, kernel_size,
                         padding='same',
                         name=conv_name_base + '2b',
                         data_format='channels_first')
    with tf.variable_scope(bn_name_base + '2b'):
        x = batch_norm(x, data_format='NCHW')
    x = tf.nn.relu(x)

    x = tf.layers.conv2d(x,
                         filters3, (1, 1),
                         name=conv_name_base + '2c',
                         data_format='channels_first')
    with tf.variable_scope(bn_name_base + '2c'):
        x = batch_norm(x, data_format='NCHW')

    shortcut = tf.layers.conv2d(input_tensor,
                                filters3, (1, 1), strides=strides,
                                name=conv_name_base + '1',
                                data_format='channels_first')
    with tf.variable_scope(bn_name_base + '1'):
        shortcut = batch_norm(shortcut, data_format='NCHW')

    x = tf.math.add(x, shortcut)
    x = tf.nn.relu(x)

    return x


def resnet50_model_fn(
             input_tensor,
             classes=28):
    """Instantiates the ResNet50 architecture.

    Optionally loads weights pre-trained on ImageNet.
    Note that the data format convention used by the model is
    the one specified in your Keras config at `~/.keras/keras.json`.

    # Arguments
        include_top: whether to include the fully-connected
            layer at the top of the network.
        weights: one of `None` (random initialization),
              'imagenet' (pre-training on ImageNet),
              or the path to the weights file to be loaded.
        input_tensor: optional Keras tensor (i.e. output of `layers.Input()`)
            to use as image input for the model.
        input_shape: optional shape tuple, only to be specified
            if `include_top` is False (otherwise the input shape
            has to be `(224, 224, 3)` (with `channels_last` data format)
            or `(3, 224, 224)` (with `channels_first` data format).
            It should have exactly 3 inputs channels,
            and width and height should be no smaller than 197.
            E.g. `(200, 200, 3)` would be one valid value.
        pooling: Optional pooling mode for feature extraction{
    "epochs": 80,

    "batch_size": 4,

    "image_size": 512,

    "num_parallel_calls": 8,
    "save_summary_steps": 100,
    "log_step_count_steps": 10,

    "train_dir": "data/train",
    "test_dir": "data/test",

    "random_seed": 32087,

    "dtype": "float16",

    "regularization_scale": 0.001,
    "loss_scale": 10000.0,

    "tta": false
}

            when `include_top` is `False`.
            - `None` means that the output of the model will be
                the 4D tensor output of the
                last convolutional layer.
            - `avg` means that global average pooling
                will be applied to the output of the
                last convolutional layer, and thus
                the output of the model will be a 2D tensor.
            - `max` means that global max pooling will
                be applied.
        classes: optional number of classes to classify images
            into, only to be specified if `include_top` is True, and
            if no `weights` argument is specified.

    # Returns
        A Keras model instance.

    # Raises
        ValueError: in case of invalid argument for `weights`,
            or invalid input shape.
    """

    x = tf.pad(input_tensor, tf.constant([[0, 0], [0, 0], [3, 3], [3, 3]]), "CONSTANT")
    x = tf.layers.conv2d(x,
                         64, (7, 7),
                         padding='valid',
                         name='conv1',
                         data_format='channels_first')
    x = batch_norm(x, data_format='NCHW')
    x = tf.nn.relu(x)
    x = tf.pad(x, tf.constant([[0, 0], [0, 0], [1, 1], [1, 1]]), "CONSTANT")
    x = tf.layers.max_pooling2d(x, (3, 3), strides=(2, 2), data_format='channels_first')

    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

    x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

    x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

    x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')

    x = tf.math.reduce_mean(x, axis=[2, 3], name='avg_pool')
    logits = tf.layers.dense(x, classes, name='fc' + str(classes))

    return logits
