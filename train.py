import argparse
import os
import logging

import json
import tables
import tensorflow as tf
from tensorflow.python import debug as tf_debug
from tensorflow.python import RunOptions
from tensorflow.python import keras

import pandas as pd

from dataload import get_dataset_h5, get_dataset_synthetic
from utils import Params
from utils import set_logger

import tf_models
import utils
import keras_models
import keras_metrics


logging.getLogger().setLevel(logging.INFO)


def main():
    cmdline = parse_cmdline()
    flags = cmdline.parse_args()

    if flags.param_file:
        param_file = flags.param_file
    else:
        param_file = os.path.join(flags.model_dir, 'params.json')
    with open(param_file, 'r') as f:
        param_dict = json.load(f)
    params = argparse.Namespace(**param_dict)

    if flags.framework == 'keras':
        main_keras(flags, params)
    elif flags.framework == 'tf':
        main_tf(flags, params)


def main_keras(flags, params):
    if params.dtype == 'float16':
        raise NotImplementedError

    n_classes = 28

    ds_train = make_input_fn(tf.estimator.ModeKeys.TRAIN, flags, params)()

    metrics = [keras_metrics.MacroF1Score(n_classes, 0.5),
               keras_metrics.TruePositives(n_classes, 0.5),
               keras_metrics.FalsePositives(n_classes, 0.5),
               keras_metrics.FalseNegatives(n_classes, 0.5),
               keras_metrics.TrueNegatives(n_classes, 0.5)]

    model = keras_models.deep_yeast_model(params)

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=metrics,
                  )

    model.fit(
        ds_train,
        steps_per_epoch=100,
        epochs=1
    )



def main_tf(flags, params):
    # model_fn = tf_models.deep_yeast_gap_model_fn
    model_fn = tf_models.resnet50_model_fn
    train_input_fn, n_train_samples = make_input_fn(tf.estimator.ModeKeys.TRAIN, flags, params)
    eval_input_fn, n_eval_samples = make_input_fn(tf.estimator.ModeKeys.EVAL, flags, params)

    n_train_batches = n_train_samples//params.batch_size
    n_eval_batches = n_eval_samples//params.batch_size

    if n_train_batches*params.batch_size < n_train_samples:
        n_train_batches += 1
    if n_eval_batches*params.batch_size < n_eval_samples:
        n_eval_batches += 1

    config = tf.estimator.RunConfig(model_dir=flags.model_dir,
                                    save_checkpoints_steps=n_train_batches,
                                    save_summary_steps=params.save_summary_steps,
                                    log_step_count_steps=params.log_step_count_steps)

    # warm_start = tf.estimator.WarmStartSettings(
    #     flags.warm_start,
    #     var_name_to_prev_var_name={
    #         'deep_yeast_gap/conv_group_0/conv0/conv2d/kernel': 'conv2d/kernel',
    #         'deep_yeast_gap/conv_group_0/conv0/conv2d/bias': 'conv2d/bias',
    #         'deep_yeast_gap/conv_group_0/conv0/beta': 'BatchNorm/beta',
    #
    #         'deep_yeast_gap/conv_group_0/conv1/conv2d/kernel': 'conv2d_1/kernel',
    #         'deep_yeast_gap/conv_group_0/conv1/conv2d/bias': 'conv2d_1/bias',
    #         'deep_yeast_gap/conv_group_0/conv1/beta': 'BatchNorm_1/beta',
    #
    #         'deep_yeast_gap/conv_group_1/conv0/conv2d/kernel': 'conv2d_2/kernel',
    #         'deep_yeast_gap/conv_group_1/conv0/conv2d/bias': 'conv2d_2/bias',
    #         'deep_yeast_gap/conv_group_1/conv0/beta': 'BatchNorm_2/beta',
    #
    #         'deep_yeast_gap/conv_group_1/conv1/conv2d/kernel': 'conv2d_3/kernel',
    #         'deep_yeast_gap/conv_group_1/conv1/conv2d/bias': 'conv2d_3/bias',
    #         'deep_yeast_gap/conv_group_1/conv1/beta': 'BatchNorm_3/beta',
    #
    #         'deep_yeast_gap/conv_group_3/conv0/conv2d/kernel': 'conv2d_4/kernel',
    #         'deep_yeast_gap/conv_group_3/conv0/conv2d/bias': 'conv2d_4/bias',
    #         'deep_yeast_gap/conv_group_3/conv0/beta': 'BatchNorm_4/beta',
    #
    #         'deep_yeast_gap/conv_group_3/conv1/conv2d/kernel': 'conv2d_5/kernel',
    #         'deep_yeast_gap/conv_group_3/conv1/conv2d/bias': 'conv2d_5/bias',
    #         'deep_yeast_gap/conv_group_3/conv1/beta': 'BatchNorm_5/beta',
    #
    #         'deep_yeast_gap/conv_group_3/conv2/conv2d/kernel': 'conv2d_6/kernel',
    #         'deep_yeast_gap/conv_group_3/conv2/conv2d/bias': 'conv2d_6/bias',
    #         'deep_yeast_gap/conv_group_3/conv2/beta': 'BatchNorm_6/beta',
    #
    #         'deep_yeast_gap/conv_group_3/conv3/conv2d/kernel': 'conv2d_7/kernel',
    #         'deep_yeast_gap/conv_group_3/conv3/conv2d/bias': 'conv2d_7/bias',
    #         'deep_yeast_gap/conv_group_3/conv3/beta': 'BatchNorm_7/beta',
    #
    #         'deep_yeast_gap/dense/kernel': 'dense/kernel',
    #         'deep_yeast_gap/dense/bias': 'dense/bias'
    #     }
    # )

    estimator = tf.estimator.Estimator(
        model_fn,
        flags.model_dir,
        params=params,
        config=config,
        # warm_start_from=warm_start
    )

    hooks = []
    if flags.debug:
        debug_hook = tf_debug.TensorBoardDebugHook("ben:6064")
        hooks.append(debug_hook)

    train_spec = tf.estimator.TrainSpec(
        train_input_fn, max_steps=params.epochs*n_train_batches, hooks=hooks
    )

    serving_input_receiver_fn = make_serving_input_receiver_fn(flags, params)
    best_exporter = tf.estimator.BestExporter(serving_input_receiver_fn=serving_input_receiver_fn)

    eval_spec = tf.estimator.EvalSpec(
        eval_input_fn, steps=n_eval_batches, hooks=hooks, exporters=[best_exporter],
        throttle_secs=1,
        start_delay_secs=0
    )

    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)
    #     estimator.train(train_input_fn, steps=n_train_batches, hooks=hooks)
    #     estimator.evaluate(eval_input_fn, steps=n_eval_batches, hooks=hooks)


def make_input_fn(mode, flags, params):
    if flags.synthetic:
        def input_fn():
            ds = get_dataset_synthetic(params)
            return ds
        n_samples = None
    else:
        h5_file = tables.open_file(flags.h5_file)

        if mode == tf.estimator.ModeKeys.TRAIN:
            group = h5_file.root.train
        elif mode == tf.estimator.ModeKeys.EVAL:
            group = h5_file.root.eval
        elif mode == tf.estimator.ModeKeys.PREDICT:
            group = h5_file.root.test
        else:
            raise ValueError('Unrecognized mode key.')

        def input_fn():
            return get_dataset_h5(group, mode == tf.estimator.ModeKeys.TRAIN, params)

        n_samples = group.images.shape[0]

    return input_fn, n_samples


def make_serving_input_receiver_fn(flags, params):
    def serving_input_receiver_fn():
        feature = tf.placeholder(params.dtype, [None, 4, params.image_size, params.image_size])
        return tf.estimator.export.TensorServingInputReceiver(
            features=feature,
            receiver_tensors=feature
        )
    return serving_input_receiver_fn


def parse_cmdline(cmdline: argparse.ArgumentParser = None):
    if not cmdline:
        cmdline = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )

    cmdline.add_argument('-m', '--model_dir', default='/media/shared/ben/code/proteinatlas/experiments/test',
                         help="""Experiment directory containing params.json""")
    cmdline.add_argument('--param_file', default=None,
                         help="""Location of params.json file (defaults to <model_dir>/params.json)""")
    cmdline.add_argument('--h5_file', required=True,
                         help="""HDF5 file with pre-split train, validation, test split images and labels.""")
    cmdline.add_argument('-d', '--data_dir', default='/media/shared/ben/code/proteinatlas//data',
                         help="""Directory containing the dataset""")
    cmdline.add_argument('--synthetic', default=False,
                         help="""Use synthetic image and label data for performance testing.""")
    cmdline.add_argument('-f', '--framework', default='keras',
                         help="""Use keras model or tensorflow estimator. Default keras.""")
    cmdline.add_argument('--debug', default=False,
                         help="""Connect to TensorBoard debugger.""")
    # cmdline.add_argument('-w', '--warm_start', required=True)
    return cmdline


if __name__ == '__main__':
    main()
    # args = parser.parse_args()
    # set_logger(os.path.join(args.model_dir, 'train.log'))
    #
    # json_path = os.path.join(args.model_dir, 'params.json')
    # params = Params(json_path)
    #
    # tf.set_random_seed(params.random_seed)
    # np.random.seed(params.random_seed)
    #
    # main(params, args)
