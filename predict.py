import argparse
import os
import logging

import numpy as np
import json
import tables
import tensorflow as tf
from tensorflow.python import debug as tf_debug
from tensorflow.python import RunOptions
from tensorflow.python import keras

import pandas as pd

from dataload import get_dataset_h5, get_dataset_synthetic
from utils import Params
from utils import set_logger

import tf_models
import utils
import keras_models
import keras_metrics


logging.getLogger().setLevel(logging.INFO)


def main():
    cmdline = parse_cmdline()
    flags = cmdline.parse_args()

    if flags.param_file:
        param_file = flags.param_file
    else:
        param_file = os.path.join(flags.model_dir, 'params.json')
    with open(param_file, 'r') as f:
        param_dict = json.load(f)
    params = argparse.Namespace(**param_dict)

    main_tf(flags, params)


def main_keras(flags, params):
    if params.dtype == 'float16':
        raise NotImplementedError

    n_classes = 28

    ds_train = make_input_fn(tf.estimator.ModeKeys.TRAIN, flags, params)()

    metrics = [keras_metrics.MacroF1Score(n_classes, 0.5),
               keras_metrics.TruePositives(n_classes, 0.5),
               keras_metrics.FalsePositives(n_classes, 0.5),
               keras_metrics.FalseNegatives(n_classes, 0.5),
               keras_metrics.TrueNegatives(n_classes, 0.5)]

    model = keras_models.deep_yeast_model(params)

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=metrics,
                  )

    model.fit(
        ds_train,
        steps_per_epoch=100,
        epochs=1
    )


def main_tf(flags, params):
    model_fn = tf_models.deep_yeast_gap_model_fn

    input_fn, n_samples = make_input_fn(flags, params)
    n_batches = n_samples//params.batch_size

    if n_batches*params.batch_size < n_samples:
        n_batches += 1

    config = tf.estimator.RunConfig(model_dir=flags.model_dir,
                                    save_checkpoints_steps=n_batches,
                                    save_summary_steps=10,
                                    log_step_count_steps=10)

    warm_start = tf.estimator.WarmStartSettings(
        flags.warm_start,
        var_name_to_prev_var_name={
            'deep_yeast_gap/conv_group_0/conv0/conv2d/kernel': 'conv2d/kernel',
            'deep_yeast_gap/conv_group_0/conv0/conv2d/bias': 'conv2d/bias',
            'deep_yeast_gap/conv_group_0/conv0/beta': 'BatchNorm/beta',

            'deep_yeast_gap/conv_group_0/conv1/conv2d/kernel': 'conv2d_1/kernel',
            'deep_yeast_gap/conv_group_0/conv1/conv2d/bias': 'conv2d_1/bias',
            'deep_yeast_gap/conv_group_0/conv1/beta': 'BatchNorm_1/beta',

            'deep_yeast_gap/conv_group_1/conv0/conv2d/kernel': 'conv2d_2/kernel',
            'deep_yeast_gap/conv_group_1/conv0/conv2d/bias': 'conv2d_2/bias',
            'deep_yeast_gap/conv_group_1/conv0/beta': 'BatchNorm_2/beta',

            'deep_yeast_gap/conv_group_1/conv1/conv2d/kernel': 'conv2d_3/kernel',
            'deep_yeast_gap/conv_group_1/conv1/conv2d/bias': 'conv2d_3/bias',
            'deep_yeast_gap/conv_group_1/conv1/beta': 'BatchNorm_3/beta',

            'deep_yeast_gap/conv_group_3/conv0/conv2d/kernel': 'conv2d_4/kernel',
            'deep_yeast_gap/conv_group_3/conv0/conv2d/bias': 'conv2d_4/bias',
            'deep_yeast_gap/conv_group_3/conv0/beta': 'BatchNorm_4/beta',

            'deep_yeast_gap/conv_group_3/conv1/conv2d/kernel': 'conv2d_5/kernel',
            'deep_yeast_gap/conv_group_3/conv1/conv2d/bias': 'conv2d_5/bias',
            'deep_yeast_gap/conv_group_3/conv1/beta': 'BatchNorm_5/beta',

            'deep_yeast_gap/conv_group_3/conv2/conv2d/kernel': 'conv2d_6/kernel',
            'deep_yeast_gap/conv_group_3/conv2/conv2d/bias': 'conv2d_6/bias',
            'deep_yeast_gap/conv_group_3/conv2/beta': 'BatchNorm_6/beta',

            'deep_yeast_gap/conv_group_3/conv3/conv2d/kernel': 'conv2d_7/kernel',
            'deep_yeast_gap/conv_group_3/conv3/conv2d/bias': 'conv2d_7/bias',
            'deep_yeast_gap/conv_group_3/conv3/beta': 'BatchNorm_7/beta',

            'deep_yeast_gap/dense/kernel': 'dense/kernel',
            'deep_yeast_gap/dense/bias': 'dense/bias'
        }
    )

    h5_file = tables.open_file(flags.h5_file)
    df = pd.DataFrame(data=h5_file.root.names[:], columns=['Id'])
    df.loc[:, "Predicted"] = ""

    hooks = []
    if flags.debug:
        debug_hook = tf_debug.TensorBoardDebugHook("ben:6064")
        hooks.append(debug_hook)

    estimator = tf.estimator.Estimator(
        model_fn,
        flags.model_dir,
        params=params,
        config=config,
        warm_start_from=warm_start
    )

    idx = 0
    for prediction in estimator.predict(input_fn, hooks=hooks):
        if idx % params.batch_size == 0:
            tf.logging.info('Batch %d of %d.', idx/params.batch_size, n_batches)
        labels, = np.nonzero(prediction['labels'])  # type: np.ndarray
        label_str = " ".join(labels.astype(str))
        df.loc[idx, "Predicted"] = label_str
        idx += 1

    df.to_csv('predictions.csv', index=False)


def make_input_fn(flags, params):
    h5_file = tables.open_file(flags.h5_file)
    group = h5_file.root

    def input_fn():
        return get_dataset_h5(group, False, params, True)

    n_samples = group.images.shape[0]

    return input_fn, n_samples


def parse_cmdline(cmdline: argparse.ArgumentParser = None):
    if not cmdline:
        cmdline = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )

    cmdline.add_argument('-m', '--model_dir', default='/media/shared/ben/code/proteinatlas/experiments/test',
                         help="""Experiment directory containing params.json""")
    cmdline.add_argument('-w', '--warm_start', required=True)
    cmdline.add_argument('--param_file', default=None,
                         help="""Location of params.json file (defaults to <model_dir>/params.json)""")
    cmdline.add_argument('--h5_file', required=True,
                         help="""HDF5 file with test images.""")
    cmdline.add_argument('--debug', default=False,
                         help="""Connect to TensorBoard debugger.""")
    return cmdline


if __name__ == '__main__':
    main()
