import logging

from tensorflow.python.keras.layers import Layer
from tensorflow.python.keras import backend as K
import numpy as np


class NClassMetric(Layer):
    def __init__(self, n_classes, name=None, **kwargs):
        super(NClassMetric, self).__init__(name=name, **kwargs)
        self.stateful = True
        self.epsilon = K.constant(K.epsilon(), dtype="float64")
        self.__name__ = name
        self.n_classes = n_classes


class TruePositives(NClassMetric):
    def __init__(self, n_classes, threshold, name='true_positives', **kwargs):
        super(TruePositives, self).__init__(n_classes, name=name, **kwargs)
        self.tp = K.variable(np.zeros(self.n_classes, dtype=np.int32), dtype="int32")
        self.threshold = threshold

    def reset_states(self):
        K.set_value(self.tp, np.zeros(self.n_classes, dtype=np.int32))

    def __call__(self, y_true, y_pred):
        y_true = K.cast(y_true, dtype="int32")
        y_pred = K.cast(y_pred > self.threshold, dtype="int32")

        tp = K.sum(y_true * y_pred, axis=0)
        current_tp = self.tp*int(1)

        tp_update = K.update_add(self.tp, tp)

        self.add_update(tp_update, inputs=[y_true, y_pred])

        return tp + current_tp


class FalsePositives(NClassMetric):
    def __init__(self, n_classes, threshold, name='false_positives', **kwargs):
        super(FalsePositives, self).__init__(n_classes, name=name, **kwargs)
        self.fp = K.variable(np.zeros(self.n_classes, dtype=np.int32), dtype="int32")
        self.threshold = threshold

    def reset_states(self):
        K.set_value(self.fp, np.zeros(self.n_classes, dtype=np.int32))

    def __call__(self, y_true, y_pred):
        y_true = K.cast(y_true, dtype="int32")
        y_pred = K.cast(y_pred > self.threshold, dtype="int32")

        fp = K.sum((1 - y_true) * y_pred, axis=0)
        current_fp = self.fp*1

        fp_update = K.update_add(self.fp, fp)

        self.add_update(fp_update, inputs=[y_true, y_pred])

        return fp + current_fp


class FalseNegatives(NClassMetric):
    def __init__(self, n_classes, threshold, name='false_negatives', **kwargs):
        super(FalseNegatives, self).__init__(n_classes, name=name, **kwargs)
        self.fn = K.variable(np.zeros(self.n_classes, dtype=np.int32), dtype="int32")
        self.threshold = threshold

    def reset_states(self):
        K.set_value(self.fn, np.zeros(self.n_classes, dtype=np.int32))

    def __call__(self, y_true, y_pred):
        y_true = K.cast(y_true, dtype="int32")
        y_pred = K.cast(y_pred > self.threshold, dtype="int32")

        fn = K.sum(y_true * (1 - y_pred), axis=0)

        current_fn = self.fn*1

        fn_update = K.update_add(self.fn, fn)

        self.add_update(fn_update, inputs=[y_true, y_pred])

        return fn + current_fn


class TrueNegatives(NClassMetric):
    def __init__(self, n_classes, threshold, name='true_negatives', **kwargs):
        super(TrueNegatives, self).__init__(n_classes, name=name, **kwargs)
        self.tn = K.variable(np.zeros(self.n_classes, dtype=np.int32), dtype="int32")
        self.threshold = threshold

    def reset_states(self):
        K.set_value(self.tn, np.zeros(self.n_classes, dtype=np.int32))

    def __call__(self, y_true, y_pred):
        y_true = K.cast(y_true, dtype="int32")
        y_pred = K.cast(y_pred > self.threshold, dtype="int32")

        tn = K.sum((1 - y_true) * (1 - y_pred), axis=0)

        current_tn = self.tn*1

        tn_update = K.update_add(self.tn, tn)

        self.add_update(tn_update, inputs=[y_true, y_pred])

        return tn + current_tn


class MacroF1Score(NClassMetric):
    def __init__(self, n_classes, threshold, name="macro_f1_score", **kwargs):
        super(MacroF1Score, self).__init__(n_classes, name=name, **kwargs)

        self.tps = TruePositives(n_classes, threshold)
        self.fps = FalsePositives(n_classes, threshold)
        self.fns = FalseNegatives(n_classes, threshold)

        self.threshold = K.constant(threshold)

    def reset_states(self):
        self.tps.reset_states()
        self.fps.reset_states()
        self.fns.reset_states()

    def __call__(self, y_true, y_pred):
        logging.info('Called MacroF1Score')
        tps = self.tps(y_true, y_pred)
        fps = self.fps(y_true, y_pred)
        fns = self.fns(y_true, y_pred)

        self.add_update(self.tps.updates)
        self.add_update(self.fps.updates)
        self.add_update(self.fns.updates)

        tps = K.cast(tps, self.epsilon.dtype)
        fps = K.cast(fps, self.epsilon.dtype)
        fns = K.cast(fns, self.epsilon.dtype)

        recalls = tps / (tps + fns + self.epsilon)
        precisions = tps / (tps + fps + self.epsilon)

        f1_scores = 2 * (precisions * recalls) / (precisions + recalls + self.epsilon)

        out = K.mean(f1_scores)
        logging.info('Computed MacroF1Score')
        return out

