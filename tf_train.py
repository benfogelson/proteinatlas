"""
Heavily drawn from the Nvidia TensorFlow mixed precision example
"""


import argparse

import tensorflow as tf

from dataload import get_dataset_synthetic


def main():
    cmdline = get_cmdline()

    global FLAGS
    FLAGS, unknown_args = cmdline.parse_known_args()


def shallow_network(input_layer, model_dtype):
    x = tf.layers.flatten(input_layer)
    x = tf.layers.dense(x, 128)
    x = tf.layers.dense(x, 28)
    return x


def get_cmdline():
    cmdline = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    cmdline.add_argument('-b', '--batch_size', default=32, type=int,
                         help="""Size of each minibatch.""")
    cmdline.add_argument('--loss_scale', default=1., type=float,
                         help="""Loss scaling factor.""")
    add_bool_argument(cmdline, '--fp16',
                      help="""Train using float16 precision.""")
    return cmdline


def add_bool_argument(cmdline, shortname, longname=None, default=False, help=None):
    if longname is None:
        shortname, longname = None, shortname
    elif default == True:
        raise ValueError("""Boolean arguments that are True by default should not have short names.""")
    name = longname[2:]
    feature_parser = cmdline.add_mutually_exclusive_group(required=False)
    if shortname is not None:
        feature_parser.add_argument(shortname, '--'+name, dest=name, action='store_true', help=help, default=default)
    else:
        feature_parser.add_argument(           '--'+name, dest=name, action='store_true', help=help, default=default)
    feature_parser.add_argument('--no'+name, dest=name, action='store_false')
    return cmdline


if __name__ == '__main__':
    main()
